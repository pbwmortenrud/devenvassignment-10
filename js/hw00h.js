'use strict';

const go = function () {
    let targets = document.getElementsByClassName('title');
    for(let i = 0; i < targets.length; ++i) {
        targets[i].innerHTML = 'Hello, World!';
    }
	
}
const answer = function () {
    let targets = document.getElementsByClassName('answer');
    for(let i = 0; i < targets.length; ++i) {
        targets[i].innerHTML = 'Hi yourself!';
    }
	
}


window.addEventListener('load', go);
window.addEventListener('load', answer);
